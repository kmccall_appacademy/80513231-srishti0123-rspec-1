def echo(phrase)
  phrase
end

def shout(phrase)
  phrase.upcase
end

def repeat(phrase, times = 2)
  Array.new(times, phrase).join(" ")
end

def start_of_word(word, i)
  word[0...i]
end

def first_word(sentence)
  words = sentence.split
  words[0]
end

def titleize(sentence)
  little_words = ["and", "over", "the"]

  words = sentence.split
  titleized = words.map.with_index do |word, i|
      if little_words.include?(word) && i != 0
      word
    else
      word.capitalize
    end
  end
  titleized.join(" ")
end
