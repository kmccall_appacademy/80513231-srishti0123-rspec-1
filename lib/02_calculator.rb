# def add(x, y)
#   x + y
# end
#
# def subtract(x, y)
#   x - y
# end
#
# def sum(arr)
#   return 0 if arr.empty?
#   arr.reduce(:+)
# end
#
# def multiply(*numbers)
#   product = 1
#   numbers.each {|num| product *= num}
#   product
# end
#
# def power(x,y)
#   x ** y
# end
#
# def factorial(int)
#   return 1 if int == 0
#   product = 1
#   int.downto(1).each do |num|
#     product *= num
#   end
#   product
# end

def add(x,y)
  x + y
end

def sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

def subtract(x, y)
  x - y
end

def multiply(*nums)
  nums.reduce(:*)
end

def factorial(num)
  return 1 if num.zero?
  (1..num).to_a.reduce(:*)
end

def power(x,y)
  x ** y
end
