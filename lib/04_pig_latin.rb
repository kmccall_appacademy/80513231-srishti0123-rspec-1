def translate(str)
  words = str.split
  words.map {|word| latinize(word)}.join(" ")
end

def latinize(word)
  vowels = 'aeiou'
  latinized = ""
  word.chars.each_with_index do |ch, i|
    next if word[i] == "u" && word[i-1] == "q"
    if vowels.include?(ch)
      latinized << word[i..-1] + word[0...i] + "ay"
      break
    end
  end
  latinized
end
